# Makefile for compiling example SA usage executable.
# Author: Shanika Kuruppu

CXX = clang++

CFLAGS = -Wall -O0 -g -DLOGLEVEL=0

INCLUDES = -I./include

LFLAGS = -L./lib

LIBS = -lsa

SRCS = main.cpp

OBJS = main.o

all: sa

sa: $(OBJS)
	$(MAKE) -C lib/src
	$(CXX) $(CFLAGS) -o $@ $^ $(LFLAGS) $(LIBS)

%.o: %.cpp
	$(CXX) $(CFLAGS) $(INCLUDES) -c $<

clean:
	rm -f $(OBJS)

clobber: clean
	rm -f sa

depend: $(SRCS)
	$(CXX) $(INCLUDES) -MM $(SRCS) > depend

include depend
