/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <map>

#include "suffix_array.h"

namespace {

template <typename T>
void SA_IS(const T& S, Array* SA);

/**
 * Returns the minimum number of bits required to represent given number.
 *
 * Zero is assumed to be represented by one bit.
 *
 * @param number Number to calculate the bit width for.
 * @return The number of bits required to represent number.
 */
uint64_t get_bit_width(uint64_t number) {
    if (!number) {
        return 1;
    }

    uint64_t width = 0;
    while (number) {
        number >>= 1;
        width += 1;
    }
    return width;
}

/**
 * Calculates the suffix types, LMS substrings and symbol boundaries.
 *
 * @param S String to calculate the information for.
 * @param t Output location for suffix type (1 for S-type, 0 for L-type).
 * @param P_1 Output for LMS substring start positions.
 * @param B Output for bucket start positions.
 * @param symbol_boundary_map Output for mapping symbols to buckets.
 */
template <typename T>
void get_suffix_classifications(const T& S, Array* t,
    std::vector<uint64_t>* P_1, std::vector<uint64_t>* B,
    std::map<uint64_t, uint64_t>* symbol_boundary_map) {
    uint64_t i;

    // Create the array that classifies each position of the string as S-type
    // or L-type. If an item is an S-type, it is set to true and if it is an
    // L-type, it is set to false.
    // S-type: S[i] < S[i+1] or S[i] = S[i+1] and suf(S, i+1) is S-type
    // L-type: S[i] > S[i+1] or S[i] = S[i+1] and suf(S, i+1) is L-type
    (*t)[S.size() - 1] = 1;
    for (i=S.size()-1; i>0; i--) {
        if ((S[i - 1] < S[i]) || (S[i - 1] == S[i] && (*t)[i])) {
            (*t)[i - 1] = 1;
        } else {
            (*t)[i - 1] = 0;
        }
    }

    // Scan t to find LMS substrings. LMS substrings are substrings S[i..j]
    // where S[i] and S[j] for i != j are LMS characters (those where S[i-1]
    // is L-type and S[i] is S-type) and there are no other LMS characters in
    // S[i+1..j-1].
    for (i=0; i+1<S.size(); i++) {
        // S[i] is L-type and S[j] is S-type
        if (!(*t)[i] && (*t)[i + 1]) {
            P_1->push_back(i + 1);
        }
    }

    // If the string only contains the sentinel add the sentinel as an LMS
    // substring.
    if (S.size() == 1) {
        P_1->push_back(0);
    }

    // Determine the suffix array boundaries
    std::map<uint64_t, uint64_t> symbol_count;
    for (i=0; i<S.size(); i++) {
        symbol_count[(uint64_t)S[i]]++;
    }
    uint64_t count = 0;
    for (std::map<uint64_t, uint64_t>::iterator it=symbol_count.begin();
         it != symbol_count.end(); ++it) {
        B->push_back(count);
        count += it->second;
        (*symbol_boundary_map)[it->first] = B->size() - 1;
    }
}

/**
 * Determines the ends of symbol buckets.
 *
 * @param B Bucket start positions.
 * @param length Length of the array.
 * @param bucket_pointers Vector to insert bucket end indexes.
 */
void get_bucket_end_pointers(const std::vector<uint64_t>& B, uint64_t length,
    std::vector<uint64_t>* bucket_pointers) {
    bucket_pointers->clear();

    // Set the bucket pointers to the ends of the buckets
    for (uint64_t i=1; i<B.size(); i++) {
        bucket_pointers->push_back(B[i] -1);
    }
    // The last pointer is the last position of SA
    bucket_pointers->push_back(length - 1);
}

/**
 * Positions the previous suffixes to the ones currently in SA baed on type.
 *
 * @param S String to calculate the information for.
 * @param t Location for suffix type (1 for S-type, 0 for L-type).
 * @param B Bucket start positions.
 * @param symbol_boundary_map Mapping of symbols to buckets.
 * @param SA Ouput for updated suffix array.
 * @param occupied_SA_idx Output for occupied positions of the suffix array.
 */
template <typename T>
void position_previous_suffixes(const T& S, const Array& t,
    const std::vector<uint64_t>& B,
    const std::map<uint64_t, uint64_t>& symbol_boundary_map,
    Array* SA, Array* occupied_SA_idx) {
    // Reset bucket pointers to start of buckets
    std::vector<uint64_t> bucket_pointers = B;

    // From left to right, for each SA[i], if S[SA[i]-1] is L-type, put
    // SA[i]-1 at current end of L-type bucket for S[SA[i]-1].
    uint64_t i, suffix, bucket_idx;
    for (i=0; i<SA->size(); i++) {
        suffix = (*SA)[i];
        if ((*occupied_SA_idx)[i] && suffix && t[suffix - 1] == 0) {
            bucket_idx = symbol_boundary_map.find(S[suffix - 1])->second;
            (*SA)[bucket_pointers[bucket_idx]] = suffix - 1;
            (*occupied_SA_idx)[bucket_pointers[bucket_idx]] = 1;
            bucket_pointers[bucket_idx]++;
        }
    }

    get_bucket_end_pointers(B, SA->size(), &bucket_pointers);

    // From right to left, for each SA[i], if S[SA[i]-1] is S-type, put
    // SA[i]-1 at current head of S-type bucket for S[SA[i]-1].
    for (i=S.size(); i>0; i--) {
        suffix = (*SA)[i - 1];
        if ((*occupied_SA_idx)[i - 1] && suffix && t[suffix - 1] == 1) {
            bucket_idx = symbol_boundary_map.find(S[suffix - 1])->second;
            (*SA)[bucket_pointers[bucket_idx]] = suffix - 1;
            (*occupied_SA_idx)[bucket_pointers[bucket_idx]] = 1;
            bucket_pointers[bucket_idx]--;
        }
    }
}

/**
 * Build SA_1 which is a suffix array of the LMS substrings of S.
 *
 * @param S The base string to derive SA_1 from.
 * @param P_1 Output for LMS substring start positions.
 * @param SA Current suffix array of S.
 * @param SA_1 Output for suffix array of S_1 (LMS substrings of S).
 */
template <typename T>
void build_SA_1(const T& S, const std::vector<uint64_t>& P_1,
    const Array* SA, Array* SA_1) {
    uint64_t i;
    std::map<uint64_t, uint64_t> suffix_P_1_map;
    for (i=0; i<P_1.size(); i++) {
        suffix_P_1_map[P_1[i]] = i;
    }

    std::map<uint64_t, uint64_t> lms_S_1_map;
    uint64_t bucket_number = 0, j, k;
    uint64_t prev_lms_start = SA->size() - 1, prev_lms_end;
    uint64_t curr_lms_start, curr_lms_end;
    bool lms_mismatch;
    // First suffix (the sentinel) is always an LMS substring
    lms_S_1_map[prev_lms_start] = bucket_number;
    for (i=1; i<SA->size(); i++) {
        // Current suffix has a prefix that's an LMS substring
        if (suffix_P_1_map.find((*SA)[i]) != suffix_P_1_map.end()) {
            // Determine the start and end of the previous LMS substring
            prev_lms_end = prev_lms_start;
            if (suffix_P_1_map[prev_lms_start] < P_1.size() - 1) {
                prev_lms_end = P_1[suffix_P_1_map[prev_lms_start] + 1];
            }

            // Determine the start and end of the current LMS substring
            curr_lms_start = (*SA)[i];
            curr_lms_end = curr_lms_start;
            if (suffix_P_1_map[curr_lms_start] < P_1.size() - 1) {
                curr_lms_end = P_1[suffix_P_1_map[curr_lms_start] + 1];
            }

            // Determine if the two LMS substrings are the same
            lms_mismatch = true;
            for (j=prev_lms_start, k=curr_lms_start;
                 j < prev_lms_end && k < curr_lms_end; j++, k++) {
                if (S[j] != S[k]) {
                    lms_mismatch = true;
                    break;
                }
                lms_mismatch = false;
            }

            // If the two LMS substrings are different, assign a new bucket
            // number to the current LMS substring.
            if (lms_mismatch) {
                bucket_number++;
            }
            lms_S_1_map[(*SA)[i]] = bucket_number;

            prev_lms_start = curr_lms_start;
        }
    }

    Array S_1(get_bit_width(bucket_number), P_1.size());
    i = 0;
    for (std::map<uint64_t, uint64_t>::iterator it=lms_S_1_map.begin();
         it != lms_S_1_map.end(); ++it, i++) {
        S_1[i] = it->second;
    }

    if (bucket_number + 1 < P_1.size()) {
        // Induce sort the LMS substrings
        SA_IS(S_1, SA_1);
    } else {
        // Compute SA_1 from S_1
        for (i=0; i<S_1.size(); i++) {
            (*SA_1)[S_1[i]] = i;
        }
    }
}

/**
 * Builds the suffix array for the given string.
 *
 * @param S String to build the suffix array on.
 * @param SA Output for suffix array.
 */
template <typename T>
void SA_IS(const T& S, Array* SA) {
    // Calculate the suffix classification information
    Array t(1, S.size());
    std::vector<uint64_t> P_1;
    std::vector<uint64_t> B;
    std::map<uint64_t, uint64_t> symbol_boundary_map;
    get_suffix_classifications(S, &t, &P_1, &B, &symbol_boundary_map);

    std::vector<uint64_t> bucket_pointers;
    get_bucket_end_pointers(B, SA->size(), &bucket_pointers);

    // Put LMS suffixes in SA at the end of bucket corresponding to first
    // character of suffix.
    Array occupied_SA_idx(1, S.size());
    uint64_t i, suffix, bucket_idx;
    for (i=0; i<P_1.size(); i++) {
        suffix = P_1[i];
        bucket_idx = symbol_boundary_map.find(S[suffix])->second;
        (*SA)[bucket_pointers[bucket_idx]] = suffix;
        occupied_SA_idx[bucket_pointers[bucket_idx]] = 1;
        bucket_pointers[bucket_idx]--;
    }

    position_previous_suffixes(S, t, B, symbol_boundary_map, SA,
                               &occupied_SA_idx);

    Array SA_1(get_bit_width(P_1.size() - 1), P_1.size());
    build_SA_1(S, P_1, SA, &SA_1);

    // Induce SA from SA_1
    SA->clear();
    occupied_SA_idx.clear();

    // Insert all suffixes from SA_1 into corresponding S-type buckets in SA
    // preserving the relative order of suffixes in SA_1.
    get_bucket_end_pointers(B, SA->size(), &bucket_pointers);
    for (i=SA_1.size(); i>0; i--) {
        suffix = P_1[SA_1[i - 1]];
        bucket_idx = symbol_boundary_map.find(S[suffix])->second;
        (*SA)[bucket_pointers[bucket_idx]] = suffix;
        occupied_SA_idx[bucket_pointers[bucket_idx]] = 1;
        bucket_pointers[bucket_idx]--;
    }

    position_previous_suffixes(S, t, B, symbol_boundary_map, SA,
                               &occupied_SA_idx);
}

}  // namespace

SuffixArray::SuffixArray(const std::string& S) {
    initialise<std::string>(S);
    SA_IS<std::string>(S, _SA);
}

SuffixArray::SuffixArray(const std::vector<uint64_t>& S) {
    initialise<std::vector<uint64_t> >(S);
    SA_IS<std::vector<uint64_t> >(S, _SA);
}

SuffixArray::SuffixArray(const Array& S) {
    initialise<Array>(S);
    SA_IS<Array>(S, _SA);
}

template <typename T>
void SuffixArray::initialise(const T& S) {
    if (S[S.size() - 1]) {
        throw SuffixArrayException("Input is not null terminated.");
    }
    uint64_t width = get_bit_width((uint64_t)S.size() - 1);
    _SA = new Array(width, S.size());
}

SuffixArray::~SuffixArray() {
    delete _SA;
}

uint64_t SuffixArray::size() {
    return _SA->size();
}

uint64_t SuffixArray::operator[](const uint64_t idx) {
    return (*_SA)[idx];
}
