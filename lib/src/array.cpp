/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <climits>
#include <limits>
#include <sstream>
#include <stdio.h>
#include <string.h>

#include "array.h"

ArrayReference::ArrayReference(Array* array, const uint64_t idx) :
    _array(array), _idx(idx) {}

ArrayReference::~ArrayReference() {}

ArrayReference::operator uint64_t() const {
    return _array->_get(_idx);
}

ArrayReference& ArrayReference::operator=(const uint64_t val) {
    _array->_set(_idx, val);
    return *this;
}

Array::Array(const uint64_t width, const uint64_t size) :
    _width(width), _size(size) {
    // ceil((width * size) / num_bits_per_byte)
    _bytes = ((_width * _size) + (CHAR_BIT - 1)) / CHAR_BIT;
    _array = new unsigned char[_bytes];
    memset(_array, 0, _bytes);
}

Array::~Array() {
    delete[] _array;
}

uint64_t Array::width() {
    return _width;
}

uint64_t Array::size() const {
    return _size;
}

ArrayReference Array::operator[](const uint64_t idx) {
    return ArrayReference(this, idx);
}

const ArrayReference Array::operator[](const uint64_t idx) const {
    // Under this case the array will never be modified
    return ArrayReference(const_cast<Array*>(this), idx);
}

void Array::clear() {
    memset(_array, 0, _bytes);
}

uint64_t Array::_get(const uint64_t idx) {
    if (idx >= _size) {
        std::stringstream stream;
        stream << "Index " << idx << " must be less than " << _size-1 << ".";
        throw ArrayException(stream.str().c_str());
    }

    uint64_t start_bit = _width * idx;
    uint64_t start_offset = start_bit & (CHAR_BIT - 1);
    uint64_t start_byte = start_bit / CHAR_BIT;
    // All end bits, types and offsets are inclusive
    uint64_t end_bit = start_bit + _width - 1;
    uint64_t end_offset = end_bit & (CHAR_BIT - 1);
    uint64_t end_byte = end_bit / CHAR_BIT;
    uint64_t val;

    if (start_byte == end_byte) {
        // Whole item is within a single byte

        // Shift value to end of byte
        val = _array[start_byte] >> (CHAR_BIT - (start_offset + _width));
        // Only retrieve the last _width bits
        val &= (0xFF >> (CHAR_BIT - _width));
    } else {
        // Item split over many bytes

        // Retrieve the first (CHAR_BIT - start_offset) bits
        val = _array[start_byte] & (0xFF >> start_offset);

        // Retrieve bytes from the range (start_byte, end_byte)
        for (uint64_t i=start_byte+1; i<end_byte; i++) {
            val <<= CHAR_BIT;
            val |= _array[i];
        }

        // Retrieve the last end_offset + 1 bits
        val <<= (end_offset + 1);
        val |= (_array[end_byte] >> (CHAR_BIT - (end_offset + 1)));
    }

    return val;
}

void Array::_set(const uint64_t idx, const uint64_t val) {
    if (idx >= _size) {
        std::stringstream stream;
        stream << idx << ": Index must be between 0 and " << _size - 1 << ".";
        throw ArrayException(stream.str().c_str());
    }

    if ((val & (std::numeric_limits<uint64_t>::max() >> (64 - _width))) != val) {
        std::stringstream stream;
        stream << "Cannot represent " << val << " with " << _width <<
        " bits.";
        throw ArrayException(stream.str().c_str());
    }

    uint64_t start_bit = _width * idx;
    uint64_t start_offset = start_bit & (CHAR_BIT - 1);
    uint64_t start_byte = start_bit / CHAR_BIT;
    // All end bits, types and offsets are inclusive
    uint64_t end_bit = start_bit + _width - 1;
    uint64_t end_offset = end_bit & (CHAR_BIT - 1);
    uint64_t end_byte = end_bit / CHAR_BIT;

    if (start_byte == end_byte) {
        // Whole item is within a single byte
        uint64_t mask = 0xFF << (CHAR_BIT - start_offset);
        mask |= (0xFF >> (start_offset + _width));
        _array[start_byte] &= mask;
        _array[start_byte] |= (val << (CHAR_BIT - _width - start_offset));
    } else {
        // Item split over many bytes

        // Insert the first (CHAR_BIT - start_offset) bits
        _array[start_byte] &= (0xFF << (CHAR_BIT - start_offset));
        _array[start_byte] |= (val >> (_width - (CHAR_BIT - start_offset)));

        // Insert bytes from the range (start_byte, end_byte)
        uint64_t shift_size;
        for (uint64_t i=start_byte+1; i<end_byte; i++) {
            shift_size = (end_byte - i - 1) * CHAR_BIT + end_offset + 1;
            _array[i] &= 0x00;
            _array[i] |= (val >> shift_size);
        }

        // Insert the last end_offset + 1 bits
        _array[end_byte] &= (0xFF >> (end_offset + 1));
        _array[end_byte] |= (val << (CHAR_BIT - (end_offset + 1)));
    }
}
