/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "array.h"
#include "suffix_array.h"
#include "gtest/gtest.h"

/**
 * Tests the suffix array constructor with string.
 */
TEST(SuffixArrayTest, ConstructorString) {
    std::string S("mmiissiissiippii");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);
}

/**
 * Tests the suffix array size.
 */
TEST(SuffixArrayTest, Size) {
    std::string S("mmiissiissiippii");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);
    EXPECT_EQ(SA.size(), S.size());
}

/**
 * Tests if an exception is thrown if input is not null terminated.
 */
TEST(SuffixArrayTest, NullTerminateExceptionString) {
    std::string S("mmiissiissiippii");

    ASSERT_THROW({SuffixArray SA(S);}, SuffixArrayException);
}

/**
 * Tests the suffix array values for a standard string.
 */
TEST(SuffixArrayTest, StandardString) {
    std::string S("mmiissiissiippii");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);

    uint64_t expected[] = {
        16, 15, 14, 10, 6, 2, 11, 7, 3, 1, 0, 13, 12, 9, 5, 8, 4
    };

    EXPECT_EQ(S.size(), SA.size());
    for (uint64_t i=0; i<SA.size(); i++) {
        EXPECT_EQ(expected[i], SA[i]);
    }
}

/**
 * Tests the suffix array of an empty string.
 */
TEST(SuffixArrayTest, EmptyString) {
    std::string S("");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);

    EXPECT_EQ(S.size(), SA.size());
    EXPECT_EQ(0, SA[0]);
}

/**
 * Tests the suffix array of a single symbol string.
 */
TEST(SuffixArrayTest, SingleSymbolString) {
    std::string S("A");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);

    uint64_t expected[] = {1, 0};

    EXPECT_EQ(S.size(), SA.size());
    for (uint64_t i=0; i<SA.size(); i++) {
        EXPECT_EQ(expected[i], SA[i]);
    }
}

/**
 * Tests the suffix array of a repetitive string.
 */
TEST(SuffixArrayTest, RepetitiveString) {
    std::string S("aaaaaaaaaa");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);

    uint64_t expected[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};

    EXPECT_EQ(S.size(), SA.size());
    for (uint64_t i=0; i<SA.size(); i++) {
        EXPECT_EQ(expected[i], SA[i]);
    }
}

/**
 * Tests the suffix array constructor with symbol vector.
 */
TEST(SuffixArrayTest, ConstructorVector) {
    uint64_t ints[] = {16, 2, 77, 29, 0};
    std::vector<uint64_t> S(ints, ints + sizeof(ints) / sizeof(uint64_t));
    SuffixArray SA(S);
}

/**
 * Tests if an exception is thrown if input is not null terminated.
 */
TEST(SuffixArrayTest, NullTerminateExceptionVector) {
    uint64_t ints[] = {16, 2, 77, 29};
    std::vector<uint64_t> S(ints, ints + sizeof(ints) / sizeof(uint64_t));

    ASSERT_THROW({SuffixArray SA(S);}, SuffixArrayException);
}

/**
 * Tests the suffix array values for a symbol vector.
 */
TEST(SuffixArrayTest, StandardVector) {
    uint64_t ints[] = {16, 2, 77, 29, 0};
    std::vector<uint64_t> S(ints, ints + sizeof(ints) / sizeof(uint64_t));
    SuffixArray SA(S);

    uint64_t expected[] = {4, 1, 0, 3, 2};

    EXPECT_EQ(S.size(), SA.size());
    for (uint64_t i=0; i<SA.size(); i++) {
        EXPECT_EQ(expected[i], SA[i]);
    }
}

/**
 * Tests the suffix array of an empty vector.
 */
TEST(SuffixArrayTest, EmptyVector) {
    std::vector<uint64_t> S;
    S.push_back(0);
    SuffixArray SA(S);

    EXPECT_EQ(S.size(), SA.size());
    EXPECT_EQ(0, SA[0]);
}

/**
 * Tests the suffix array constructor with symbol Array.
 */
TEST(SuffixArrayTest, ConstructorArray) {
    uint64_t bits = 7;
    uint64_t length = 5;
    Array S(bits, length);
    uint64_t ints[] = {16, 2, 77, 29, 0};
    for (uint64_t i=0; i<length; i++) {
        S[i] = ints[i];
    }
    SuffixArray SA(S);
}

/**
 * Tests if an exception is thrown if input is not null terminated.
 */
TEST(SuffixArrayTest, NullTerminateExceptionArray) {
    uint64_t bits = 7;
    uint64_t length = 4;
    Array S(bits, length);
    uint64_t ints[] = {16, 2, 77, 29};
    for (uint64_t i=0; i<length; i++) {
        S[i] = ints[i];
    }

    ASSERT_THROW({SuffixArray SA(S);}, SuffixArrayException);
}

/**
 * Tests the suffix array values for a symbol Array.
 */
TEST(SuffixArrayTest, StandardArray) {
    uint64_t bits = 7;
    uint64_t length = 5;
    Array S(bits, length);
    uint64_t ints[] = {16, 2, 77, 29, 0};
    for (uint64_t i=0; i<length; i++) {
        S[i] = ints[i];
    }
    SuffixArray SA(S);

    uint64_t expected[] = {4, 1, 0, 3, 2};

    EXPECT_EQ(S.size(), SA.size());
    for (uint64_t i=0; i<SA.size(); i++) {
        EXPECT_EQ(expected[i], SA[i]);
    }
}

/**
 * Tests the suffix array of an empty Array.
 */
TEST(SuffixArrayTest, EmptyArray) {
    Array S(1, 1);
    S[0] = 0;
    SuffixArray SA(S);

    EXPECT_EQ(S.size(), SA.size());
    EXPECT_EQ(0, SA[0]);
}
