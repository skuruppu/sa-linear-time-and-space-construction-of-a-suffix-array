/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <climits>

#include "array.h"
#include "gtest/gtest.h"

/**
 * Tests set and get with item width equivalent to byte width.
 */
TEST(ArrayTest, ByteWidth) {
    uint64_t bits = CHAR_BIT;
    // Test all possible values
    uint64_t length = 0x1 << bits;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected++) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    // Overwrite existing items and ensure that the new items are stored
    // correctly.
    for (uint64_t i=0, expected=length-1; i<length; i++, expected--) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }
}

/**
 * Tests set and get with item width equivalent to half a byte width.
 */
TEST(ArrayTest, HalfByteWidth) {
    uint64_t bits = CHAR_BIT / 2;
    // Test all possible values
    uint64_t length = 0x1 << bits;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected++) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    // Overwrite existing items and ensure that the new items are stored
    // correctly.
    for (uint64_t i=0, expected=length-1; i<length; i++, expected--) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }
}

/**
 * Tests set and get with item width being less than a byte width by an
 * arbitray amount.
 */
TEST(ArrayTest, FiveBitWidth) {
    uint64_t bits = 5;
    // Test all possible values
    uint64_t length = 0x1 << bits;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected++) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    // Overwrite existing items and ensure that the new items are stored
    // correctly.
    for (uint64_t i=0, expected=length-1; i<length; i++, expected--) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }
}

/**
 * Tests set and get with item width being greater than a byte width by an
 * arbitray amount.
 */
TEST(ArrayTest, LongBitWidth) {
    uint64_t bits = 11;
    // Test all possible values
    uint64_t length = 100;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected+=11) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    // Overwrite existing items and ensure that the new items are stored
    // correctly.
    for (uint64_t i=0, expected=1733; i<length; i++, expected-=13) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }
}

/**
 * Tests set and get with item width being greater than two byte widths.
 */
TEST(ArrayTest, ManyByteWidth) {
    uint64_t bits = 33;
    // Test all possible values
    uint64_t length = 100;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected+=1117) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    // Overwrite existing items and ensure that the new items are stored
    // correctly.
    for (uint64_t i=0, expected=2971215073; i<length; i++, expected-=3571) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }
}

/**
 * Tests that passing in indices greater than the specified size of the array
 * results in an ArrayException.
 */
TEST(ArrayTest, GetExceptions) {
    Array array(3, 10);

    ASSERT_THROW({uint64_t value = array[20];}, ArrayException);
}

/**
 * Tests that passing in indices greater than the specified size of the array
 * and setting to items greater than that can be held by the specified width
 * resultsin an ArrayException.
 */
TEST(ArrayTest, SetExceptions) {
    Array array(3, 10);

    // Index greater than the size of the array
    ASSERT_THROW({array[10] = 5;}, ArrayException);

    // Value larger than can be handled by the given number of bits
    ASSERT_THROW({array[3] = 9;}, ArrayException);
}

/**
 * Tests if an Array can be const.
 */
TEST(ArrayTest, ConstReference) {
    Array array(1, 1);
    array[0] = 1;

    const Array& const_array = array;
    const uint64_t value = const_array[0];

    EXPECT_EQ(array[0], value);
}

/**
 * Tests if an Array can be cleared.
 */
TEST(ArrayTest, ClearArray) {
    uint64_t bits = 1;
    uint64_t length = 0x1 << bits;
    uint64_t actual;

    Array array(bits, length);

    for (uint64_t i=0, expected=0; i<length; i++, expected++) {
        array[i] = expected;
        actual = array[i];
        EXPECT_EQ(expected, actual);
    }

    array.clear();

    for (uint64_t i=0; i<length; i++) {
        EXPECT_EQ(0, array[i]);
    }
}
