/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <exception>
#include <inttypes.h>

#ifndef INCLUDE_ARRAY_H
#define INCLUDE_ARRAY_H

class Array;

/**
 * Supports operator[] overloading for Array class.
 */
class ArrayReference
{

public:

    /**
     * Constructor.
     *
     * @param array Array in question.
     * @param idx Array index of interest.
     */
    ArrayReference(Array* array, const uint64_t idx);

    /**
     * Destructor.
     */
    ~ArrayReference();

    /**
     * Overload for casting to uint64_t type.
     */
    operator uint64_t() const;

    /**
     * Overload for asignment.
     *
     * @param val Value to store in array.
     * @return Reference to location the value was written to.
     */
    ArrayReference& operator=(const uint64_t val);

private:

    // Pointer to array to manipulate.
    Array* _array;

    // Array location to manipulate.
    uint64_t _idx;

};

/**
 * Array for storing items using a custom number of bits per item.
 */
class Array {

    friend class ArrayReference;

public:

    /**
     * Constructor.
     *
     * @param width Width of each item in the array.
     * @param size Length of the array.
     */
    Array(const uint64_t width, const uint64_t size);

    /**
     * Destructor.
     */
    ~Array();

    /**
     * @return The width of each item.
     */
    uint64_t width();

    /**
     * @return The length of the array.
     */
    uint64_t size() const;

    /**
     * Retrieve from or assign to given array index.
     *
     * @param idx Index to read from or write to.
     * @return Reference to the position in the array.
     */
    ArrayReference operator[](const uint64_t idx);

    /**
     * Retrieve from a given array index.
     *
     * @param idx Index to read from.
     * @return Const reference to the position in the array.
     */
    const ArrayReference operator[](const uint64_t idx) const;

    /**
     * Clears the array by setting all bytes to 0.
     */
    void clear();

private:

    // Array.
    unsigned char* _array;

    // Width of each item in the array.
    uint64_t _width;

    // Length of the array.
    uint64_t _size;

    // Number of bytes required for _array.
    uint64_t _bytes;

    /**
     * Retrieves value at given index.
     *
     * @param idx Index to get value from.
     * @return Value at position index of array.
     * @throws OutOfBoundsException When index is outside the array range.
     */
    uint64_t _get(const uint64_t idx);

    /**
     * Sets value at given index.
     *
     * @param idx Index to get value from.
     * @pram val Value to set.
     * @throws OutOfBoundsException When index is outside the array range.
     */
    void _set(const uint64_t idx, const uint64_t val);

};

/**
 * Exception to throw on Array related errors.
 */
class ArrayException: public std::exception
{

public:

    /**
     * Constructor.
     *
     * @param message Error message.
     */
    ArrayException(const char* message) :
        _message(message) {}

    /**
     * Returns the error describing the exception.
     *
     * @return The error string.
     */
    virtual const char* what() const throw() {
        return _message;
    }

private:

    const char* _message;

};

#endif
