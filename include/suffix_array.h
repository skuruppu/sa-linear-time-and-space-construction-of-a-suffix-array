/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <vector>

#include "array.h"

#ifndef INCLUDE_SUFFIX_ARRAY_H
#define INCLUDE_SUFFIX_ARRAY_H

/**
 * Constructs a suffix array in linear time and space.
 *
 * Based on Nong, Ge; Zhang, Sen; Chan, Wai Hong (2009). "Linear Suffix Array
 * Construction by Almost Pure Induced-Sorting". 2009 Data Compression
 * Conference. p. 193. doi:10.1109/DCC.2009.42.
 */
class SuffixArray {

public:

    /**
     * Constructor.
     *
     * @param S Null terminated string to construct suffix array for.
     */
    SuffixArray(const std::string& S);

    /**
     * Constructor.
     *
     * @param S Zero terminated symbol vector to construct suffix array for.
     */
    SuffixArray(const std::vector<uint64_t>& S);

    /**
     * Constructor.
     *
     * @param S Zero terminated symbol Array to construct suffix array for.
     */
    SuffixArray(const Array& S);

    /**
     * Destructor.
     */
    ~SuffixArray();

    /**
     * @return The length of the suffix array.
     */
    uint64_t size();

    /**
     * Retrieve the item at the given array index.
     *
     * @param idx Index to read from or write to.
     * @return Item at the given position in the array.
     */
    uint64_t operator[](const uint64_t idx);

private:

    // The suffix array.
    Array* _SA;

    template<typename T>
    void initialise(const T& S);

};

/**
 * Exception to throw on SuffixArray related errors.
 */
class SuffixArrayException: public std::exception
{

public:

    /**
     * Constructor.
     *
     * @param message Error message.
     */
    SuffixArrayException(const char* message) :
        _message(message) {}

    /**
     * Returns the error describing the exception.
     *
     * @return The error string.
     */
    virtual const char* what() const throw() {
        return _message;
    }

private:

    const char* _message;
};

#endif
