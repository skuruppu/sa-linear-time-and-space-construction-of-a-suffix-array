SA -- Linear time and space construction of a suffix array
==========================================================

A C++ implementation of the linear time and space suffix array construction
algorithm described in *Nong, Ge; Zhang, Sen; Chan, Wai Hong (2009). "Linear
Suffix Array Construction by Almost Pure Induced-Sorting". 2009 Data
Compression Conference. p. 193. doi:10.1109/DCC.2009.42.*

The implementation supports suffix array construction for strings of arbitrary
alphabets.

Installation
------------

To use the suffix array source as a library, follow these steps:

1. Download the source.

2. Navigate to the downloaded source. We'll create an environment variable
so we can refer to it throughout the example.

        SA_DIR="<where you downloaded the source to>"

2. Build the archive. The `libsa.a` archive will be added to the `lib/`
directory.

        make -C lib/src


3. Update the C++ include and library path environment variables.
Alternatively you can specify these in your Makefile. Look at
`$SA_DIR/Makefile` for an example.

        export CPLUS_INCLUDE_PATH=$SA_DIR/include:$CPLUS_INCLUDE_PATH
        export LIBRARY_PATH=$SA_DIR/lib:$LIBRARY_PATH

4. Update your make command to include the `-lsa` option. Once again, look at
`$SA_DIR/Makefile` for an example.

Usage
-----

A suffix array can be constructed for a string of type `std::string`, a
`std::vector<uint64_t>` of arbitrary symbols or of the custom `Array` type
provided in the source to represent an unsigned integer array using a custom
number of bits per item.

To construct a suffix array for a string:

    std::string S("mmiissiissiippii");
    S.resize(S.size() + 1, '\0');
    SuffixArray SA(S);

To construct a suffix array for a vector of integers:

    uint64_t ints[] = {16, 2, 77, 29, 0};
    std::vector<uint64_t> S(ints, ints + sizeof(ints) / sizeof(uint64_t));
    SuffixArray SA(S);

To construct a suffix array for an Array of integers:

    uint64_t bits = 7;
    uint64_t length = 5;
    Array S(bits, length);
    uint64_t ints[] = {16, 2, 77, 29, 0};
    for (uint64_t i=0; i<length; i++) {
        S[i] = ints[i];
    }
    SuffixArray SA(S);

API
---

    /**
     * Constructor.
     *
     * @param S Null terminated string to construct suffix array for.
     */
    SuffixArray(const std::string& S);

    /**
     * Constructor.
     *
     * @param S Zero terminated symbol vector to construct suffix array for.
     */
    SuffixArray(const std::vector<uint64_t>& S);

    /**
     * Constructor.
     *
     * @param S Zero terminated symbol Array to construct suffix array for.
     */
    SuffixArray(const Array& S);

    /**
     * Destructor.
     */
    ~SuffixArray();

    /**
     * @return The length of the suffix array.
     */
    uint64_t size();

    /**
     * Retrieve the item at the given array index.
     *
     * @param idx Index to read from or write to.
     * @return Item at the given position in the array.
     */
    uint64_t operator[](const uint64_t idx);

Testing
-------

Install the Google C++ Testing Framework from
[https://code.google.com/p/googletest](https://code.google.com/p/googletest)

Update `GTEST_DIR` variable in `lib/src/tests/Makefile` to point to the
directory where you installed the test framework.

To run the tests:

    make -C lib/src test
