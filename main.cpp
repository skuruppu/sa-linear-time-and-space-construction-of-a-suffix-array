/* SA
 * Copyright (C) 2014 Shanika Kuruppu
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Shanika Kuruppu nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE * DISCLAIMED. IN NO EVENT SHALL Shanika Kuruppu BE
 * LIABLE FOR ANY * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND * ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT * (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS * SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "suffix_array.h"

/**
 * Exception to throw when a file cannot be read.
 */
class FileReadException: public std::exception
{

public:

    /**
     * Constructor.
     *
     * @param file_name Name of file that cannot be read.
     */
    FileReadException(const char* file_name) :
        _file_name(file_name) {
    }

    /**
     * Returns the error describing the exception.
     *
     * @return The error string.
     */
    virtual const char* what() const throw() {
        std::stringstream stream;
        stream << "Unable to read file " << _file_name << ".";
        return stream.str().c_str();
    }

private:

    const char* _file_name;

};

/**
 * Returns the contents of the given file as a string.
 *
 * @param file_name Name of file to be read.
 * @return Contents of the file as a string.
 * @throws FileReadException If file cannot be read.
 */
std::string read_string(const char* file_name) {
    FileReadException file_exception(file_name);

    std::ifstream file(file_name, std::ifstream::in);
    if (!file.good()) {
        throw file_exception;
    }

    std::string str;
    file.seekg(0, std::ios::end);
    // Allow for n+1 bytes (including sentinel)
    str.resize((uint64_t)file.tellg() + 1, '\0');
    file.seekg(0, std::ios::beg);
    file.read(&str[0], str.size() - 1);
    file.close();

    return str;
}

int main(const int argc, const char** argv) {
    if (argc < 2) {
        std::cerr << "Usage: sa <file name>" << std::endl;
    }

    std::string str = read_string(argv[1]);

    SuffixArray sa(str);

    return 0;
}
